import { createRouter, createWebHistory } from 'vue-router'
import ExpenseForm from '@/components/ExpenseForm.vue'
import ExpenseItem from '@/components/ExpenseItem.vue'
import ExpenseList from '@/components/ExpenseList.vue'

const routes = [
  {
    path: '/form',
    name: 'form',
    component: ExpenseForm
  },
  {
    path: '/item',
    name: 'item',
    component: ExpenseItem
  },
  {
    path: '/list',
    name: 'list',
    component: ExpenseList
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
